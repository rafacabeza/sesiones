<?php

require_once 'Usuario.php';
require_once 'Vista.php';

$nombre = "";
$password = "";
$mensaje = "";
if (isset($_POST['nombre'])) {
    $nombre = $_POST['nombre'];
    $password = $_POST['password'];
    if (Usuario::validar($nombre, $password)) {
        setcookie("visitante[nombre]", $nombre);
        setcookie("visitante[password]", $password);
        setcookie("visitante[fecha]", time());
        Vista::bienvenido($nombre);
    } else {
        $mensaje= 'Usuario o contraseña falsos <br>';
        Vista::login("", "", $mensaje);
    }
} else {
    if (isset($_COOKIE['visitante'])) {
        $nombre = $_COOKIE['visitante']['nombre'];
        $password = $_COOKIE['visitante']['password'];
        $fecha = $_COOKIE['visitante']['fecha'];
        $mensaje = "Su último login fue: " . date('d-m-y h:n:s', $fecha);
    }
    Vista::login($nombre, $password, $mensaje);
}
